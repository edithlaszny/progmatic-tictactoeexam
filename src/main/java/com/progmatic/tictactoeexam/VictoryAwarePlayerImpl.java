package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;

/**
 *
 * @author hlasznyedit
 */
public class VictoryAwarePlayerImpl extends Player {
    
    public VictoryAwarePlayerImpl(PlayerType p) {
        super(p);
    }
    
}
