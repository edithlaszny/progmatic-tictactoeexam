package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author hlasznyedit
 */
public class Player extends AbstractPlayer {

    public Player(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
}
