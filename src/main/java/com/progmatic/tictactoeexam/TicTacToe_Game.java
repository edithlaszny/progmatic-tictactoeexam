package com.progmatic.tictactoeexam;

import java.util.ArrayList;
import java.util.List;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;

public class TicTacToe_Game implements Board
{
    private PlayerType [][] myBoard = {
            { PlayerType.EMPTY, PlayerType.EMPTY, PlayerType.EMPTY },
            { PlayerType.EMPTY, PlayerType.EMPTY, PlayerType.EMPTY },
            { PlayerType.EMPTY, PlayerType.EMPTY, PlayerType.EMPTY },
                                      };
    
    public TicTacToe_Game()
    {
        
    }   //  end of constructor()
    
    public TicTacToe_Game(String[][] board)
    {
        setBoard(board) ;
        
    }   //  end of constructor()
    
    public  void setBoard(String[][] board)
    {
        for (int i = 0 ; i < 3 ; i++)
        {
            for (int j = 0 ; j < 3 ; j++) 
            {
                this.myBoard[i][j] = cvtStringToPlayerType(board[i][j]) ;                
            }
        }
        
    }   //  end of method setBoard()
    
    private PlayerType cvtStringToPlayerType(String s)
    {
        PlayerType retVal ;
        
        switch (s) 
        {
            case "X":
                retVal = PlayerType.X;
                break;
            case "O":
                retVal = PlayerType.O;
                break;
        default:
                retVal = PlayerType.EMPTY;
        }

        return retVal ;

    }   //  end of method cvtStringToPlaerType()
    
    /**
     * Returns the Cell's content at rowIdx, colIdx;
     * @param rowIdx
     * @param colIdx
     * @return
     * @throws CellException if rowIdx or colIdx points to a non-existent index.
     */
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException 
    {
        if (rowIdx > 2 || colIdx > 2)
        {
            throw new CellException(rowIdx, colIdx, "Invalid row/column index") ;
        }
        
        return this.myBoard[rowIdx][colIdx] ;
        
    }   //  end of method getCell()

    /**
     * Puts a cell to the board.
     * @param cell
     * @throws CellException if rowIdx or colIdx points to a non-existent index or if the cell is non empty.
     */
    public void put(Cell cell) throws CellException 
    {
        if (cell.getRow() > 2 || cell.getCol() > 2)
        {
            throw new CellException(cell.getCol(), 
                                    cell.getRow(), 
                                    "Invalid row/column index") ;
        }
        
        if (this.myBoard[cell.getRow()][cell.getCol()].equals(PlayerType.O) ||
            this.myBoard[cell.getRow()][cell.getCol()].equals(PlayerType.X))
        {
            throw new CellException(cell.getCol(), 
                    cell.getRow(), 
                    "Cell is already occupied") ;
        }
        else
        {
            this.myBoard[cell.getRow()][cell.getCol()] = cell.getCellsPlayer() ;
        }

    }   //  end of method put()

    /**
     * Determines if player p has won.
     * A player wins if she has 3 marks in the board next to each other
     * in a horizontal, vertical or diagonal direction.
     * @param p
     * @return true if p has 3 marks in the board next to each other, false otherwise.
     */
    public boolean hasWon(PlayerType p) 
    {
        boolean retVal = false ;
        
        if (hasWonRow(0, p) || 
            hasWonRow(1, p) ||
            hasWonRow(2, p) ||
            hasWonCol(0, p) ||
            hasWonCol(1, p) ||
            hasWonCol(2, p) ||
            hasWonDiag(  p)) 
        {
            retVal = true ;
        }
        
        return retVal ;
        
    }   //  end of method hasWon()

    private boolean hasWonRow(int row, PlayerType p)
    {
        boolean retVal = false ;
        
        if (this.myBoard[row][0].equals(p) &&
            this.myBoard[row][1].equals(p) &&
            this.myBoard[row][2].equals(p))
        {
            retVal = true ;
        }
        
        return retVal ;
        
    }   //  end of method hasWonRow()
    
    private boolean hasWonCol(int col, PlayerType p)
    {
        boolean retVal = false ;
        
        if (this.myBoard[0][col].equals(p) &&
            this.myBoard[1][col].equals(p) &&
            this.myBoard[2][col].equals(p))
        {
            retVal = true ;
        }
        
        return retVal ;
        
    }   //  end of method hasWonCol()
    
    private boolean hasWonDiag(PlayerType p)
    {
        boolean retVal = false ;
        
        if ((this.myBoard[0][0].equals(p)  &&
             this.myBoard[1][1].equals(p)  &&
             this.myBoard[2][2].equals(p)) ||
                
            (this.myBoard[2][0].equals(p)  &&
             this.myBoard[1][1].equals(p)  &&
             this.myBoard[0][2].equals(p)) 
           )
        {
            retVal = true ;
        }
        
        return retVal ;
        
    }   //  end of method hasWonDiag()
    
    /**
     * Returns a list of the cells not yet occupied by any player.
     * @return a list of the cells not yet occupied by any player.
     * If there are no more empty cells on the board returns an empty list.
     */
    public List<Cell> emptyCells()
    {
        List<Cell> eCells = new ArrayList<>() ;  //  .add

        for (int i = 0 ; i < 3 ; i++)
        {
            for (int j = 0 ; j < 3 ; j++) 
            {
                if (this.myBoard[i][j].equals(PlayerType.EMPTY))
                {
                    eCells.add(new Cell(i, j, this.myBoard[i][j])) ;
                }
            }
        }
        
        return eCells ;
 
    }   //  end of method emptyCells()
        
    /**
     * Returns the player's next move.
     * This method should not modify the parameter b.
     * @param b
     * @return the player's next move. The returned cell should be an empty cell on b.
     * If there are no more possible moves returns null.
     */
    public Cell nextMove(Board b)
    {
        
        return null ;
        
    }   //  end of method nextMove()

    /**
     * Returns the PlayerType associated with this Player.
     * It should be either PlayerType.X or PlayerType.O,
     * @return the PlayerType associated with this Player.
     */
    public PlayerType getMyType() 
    {

        return null ;
    }
        
}   //  end of class TicTacToe_Game()